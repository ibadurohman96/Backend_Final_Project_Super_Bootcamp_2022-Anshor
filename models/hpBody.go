package models

import "time"

type HpBody struct {
	ID          uint      `gorm:"primary_key" json:"id"`
	Dimensions  string    `json:"dimensions"`
	Weight      string    `json:"weight"`
	Build       string    `json:"build"`
	SIM         string    `json:"sim"`
	HandphoneID string    `json:"handphone_id"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	// migration relation
	Handphone Handphone `json:"-"`
}
