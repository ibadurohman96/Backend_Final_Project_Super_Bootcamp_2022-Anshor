package models

import "time"

type HpPlatform struct {
	ID          uint      `gorm:"primary_key" json:"id"`
	OS          string    `json:"os"`
	Chipset     string    `json:"chipset"`
	CPU         string    `json:"cpu"`
	GPU         string    `json:"gpu"`
	HandphoneID string    `json:"handphone_id"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	// migration relation
	Handphone Handphone `json:"-"`
}
