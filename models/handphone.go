package models

import "time"

type Handphone struct {
	ID        uint      `json:"id" gorm:"primary_key"`
	Image     string    `json:"image"`
	Brand     string    `json:"brand"`
	Type      string    `json:"type"`
	Year      string    `json:"year"`
	Video     string    `json:"video"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	// migration relation
	HpBodies    []HpBody     `json:"-"`
	HpDisplays  []HpDisplay  `json:"-"`
	HpMemories  []HpMemory   `json:"-"`
	HpPlatforms []HpPlatform `json:"-"`
}
