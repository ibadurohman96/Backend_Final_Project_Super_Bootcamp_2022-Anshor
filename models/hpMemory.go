package models

import "time"

type HpMemory struct {
	ID          uint      `json:"id" gorm:"primary_key"`
	Card_Slot   string    `json:"card_slot"`
	Internal    string    `json:"internal"`
	HandphoneID string    `json:"handphone_id"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	// migration relation
	Handphone Handphone `json:"-"`
}
