package models

import "time"

type HpDisplay struct {
	ID          uint      `gorm:"primary_key" json:"id"`
	Type        string    `json:"type"`
	Size        string    `json:"size"`
	Resolutions string    `json:"resolutions"`
	Protections string    `json:"protections"`
	HandphoneID string    `json:"handphone_id"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	//migration relation
	Handphone Handphone `json:"-"`
}
