package routes

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"api-heroku/controllers"
	"api-heroku/middlewares"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowHeaders = []string{"Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization"}

	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true

	// OPTIONS method for ReactJS
	corsConfig.AddAllowMethods("OPTIONS")

	r.Use(cors.New(corsConfig))

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	r.POST("/register", controllers.Register)
	r.POST("/login", controllers.Login)
	r.PATCH("/change-password", controllers.ChangePassword)

	r.GET("/handphone", controllers.GetAllHandphone)
	r.GET("/handphone/:id", controllers.GetHandphoneById)
	r.GET("/handphone/:id/hp-bodies", controllers.GetHpBodiesByHandphoneId)
	r.GET("/handphone/:id/hp-displays", controllers.GetHpDisplaysByHandphoneId)
	r.GET("/handphone/:id/hp-memories", controllers.GetHpMemoriesByHandphoneId)
	r.GET("/handphone/:id/hp-platforms", controllers.GetHpPlatformsByHandphoneId)

	handphoneMiddlewareRoute := r.Group("/handphone")
	handphoneMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	{
		handphoneMiddlewareRoute.POST("/", controllers.CreateHandphone)
		handphoneMiddlewareRoute.PATCH("/:id", controllers.UpdateHandphone)
		handphoneMiddlewareRoute.DELETE("/:id", controllers.DeleteHandphone)
	}

	r.GET("/hp-body", controllers.GetAllHpBody)
	r.GET("/hp-body/:id", controllers.GetHpBodyById)

	hpBodyMiddlewareRoute := r.Group("/hp-body")
	hpBodyMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	{
		hpBodyMiddlewareRoute.POST("/", controllers.CreateHpBody)
		hpBodyMiddlewareRoute.PATCH("/:id", controllers.UpdateHpBody)
		hpBodyMiddlewareRoute.DELETE("/:id", controllers.DeleteHpBody)
	}

	r.GET("/hp-display", controllers.GetAllHpDisplay)
	r.GET("/hp-display/:id", controllers.GetHpDisplayById)

	hpDisplayMiddlewareRoute := r.Group("/hp-display")
	hpDisplayMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	{
		hpDisplayMiddlewareRoute.POST("/", controllers.CreateHpDisplay)
		hpDisplayMiddlewareRoute.PATCH("/:id", controllers.UpdateHpDisplay)
		hpDisplayMiddlewareRoute.DELETE("/:id", controllers.DeleteHpDisplay)
	}

	r.GET("/hp-memory", controllers.GetAllHpMemory)
	r.GET("/hp-memory/:id", controllers.GetHpMemoryById)

	hpMemoryMiddlewareRoute := r.Group("/hp-memory")
	hpMemoryMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	{
		hpMemoryMiddlewareRoute.POST("/", controllers.CreateHpMemory)
		hpMemoryMiddlewareRoute.PATCH("/:id", controllers.UpdateHpMemory)
		hpMemoryMiddlewareRoute.DELETE("/:id", controllers.DeleteHpMemory)
	}

	r.GET("/hp-platform", controllers.GetAllHpPlatform)
	r.GET("/hp-platform/:id", controllers.GetHpPlatformById)

	hpPlatformMiddlewareRoute := r.Group("/hp-platform")
	hpPlatformMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
	{
		hpPlatformMiddlewareRoute.POST("/", controllers.CreateHpPlatform)
		hpPlatformMiddlewareRoute.PATCH("/:id", controllers.UpdateHpPlatform)
		hpPlatformMiddlewareRoute.DELETE("/:id", controllers.DeleteHpPlatform)
	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
