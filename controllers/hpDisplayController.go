package controllers

import (
	"net/http"
	"time"

	"api-heroku/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type hpDisplayInput struct {
	Type        string `json:"type"`
	Size        string `json:"size"`
	Resolutions string `json:"resolutions"`
	Protections string `json:"protections"`
	HandphoneID string `json:"handphone_id"`
}

// GetAllHpDisplay godoc
// @Summary Get all HpDisplay.
// @Description Get a list of HpDisplay.
// @Tags HpDisplay
// @Produce json
// @Success 200 {object} []models.HpDisplay
// @Router /hp-display [get]
func GetAllHpDisplay(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var ratings []models.HpDisplay
	db.Find(&ratings)

	c.JSON(http.StatusOK, gin.H{"data": ratings})
}

// GetHpDisplayById godoc
// @Summary Get HpDisplay.
// @Description Get an HpDisplay by id.
// @Tags HpDisplay
// @Produce json
// @Param id path string true "HpDisplay id"
// @Success 200 {object} models.HpDisplay
// @Failure 400 {object} models.HpDisplay
// @Router /hp-display/{id} [get]
func GetHpDisplayById(c *gin.Context) { // Get model if exist
	var hpDisplay models.HpDisplay

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&hpDisplay).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": hpDisplay})
}

// CreateHpDisplay godoc
// @Summary Create New HpDisplay.
// @Description Creating a new HpDisplay.
// @Tags HpDisplay
// @Param Body body hpDisplayInput true "the body to create a new HpDisplay"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.HpDisplay
// @Router /hp-display [post]
func CreateHpDisplay(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// Validate input
	var input hpDisplayInput
	var handphone models.Handphone
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := db.Where("id = ?", input.HandphoneID).First(&handphone).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "handphoneID not found!"})
		return
	}

	// Create Rating
	rating := models.HpDisplay{Type: input.Type, Size: input.Size, Resolutions: input.Resolutions, Protections: input.Protections, HandphoneID: input.HandphoneID}

	db.Create(&rating)

	c.JSON(http.StatusOK, gin.H{"data": rating})
}

// UpdateHpDisplay godoc
// @Summary Update HpDisplay.
// @Description Update HpDisplay by id.
// @Tags HpDisplay
// @Produce json
// @Param id path string true "HpDisplay id"
// @Param Body body hpDisplayInput true "the body to update hp display"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.HpDisplay
// @Router /hp-display/{id} [patch]
func UpdateHpDisplay(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var rating models.HpDisplay
	var handphone models.Handphone
	if err := db.Where("id = ?", c.Param("id")).First(&rating).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input hpDisplayInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := db.Where("id = ?", input.HandphoneID).First(&handphone).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "handphoneID not found!"})
		return
	}

	var updatedInput models.HpDisplay
	updatedInput.Type = input.Type
	updatedInput.Size = input.Size
	updatedInput.Resolutions = input.Resolutions
	updatedInput.Protections = input.Protections
	updatedInput.HandphoneID = input.HandphoneID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&rating).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": rating})
}

// DeleteHpDisplay godoc
// @Summary Delete one HpDisplay.
// @Description Delete a HpDisplay by id.
// @Tags HpDisplay
// @Produce json
// @Param id path string true "HpDisplay id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /hp-display/{id} [delete]
func DeleteHpDisplay(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var rating models.HpDisplay
	if err := db.Where("id = ?", c.Param("id")).First(&rating).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&rating)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
