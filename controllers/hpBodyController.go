package controllers

import (
	"net/http"
	"time"

	"api-heroku/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type hpBodyInput struct {
	Dimensions  string `json:"dimensions"`
	Weight      string `json:"weight"`
	Build       string `json:"build"`
	SIM         string `json:"sim"`
	HandphoneID string `json:"handphone_id"`
}

// GetAllHpBody godoc
// @Summary Get all HpBody.
// @Description Get a list of HpBody.
// @Tags HpBody
// @Produce json
// @Success 200 {object} []models.HpBody
// @Router /hp-body [get]
func GetAllHpBody(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var ratings []models.HpBody
	db.Find(&ratings)

	c.JSON(http.StatusOK, gin.H{"data": ratings})
}

// GetHpBodyById godoc
// @Summary Get HpBody.
// @Description Get an HpBody by id.
// @Tags HpBody
// @Produce json
// @Param id path string true "HpBody id"
// @Success 200 {object} models.HpBody
// @Failure 400 {object} models.HpBody
// @Router /hp-body/{id} [get]
func GetHpBodyById(c *gin.Context) { // Get model if exist
	var hpBody models.HpBody

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&hpBody).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": hpBody})
}

// CreateHpBody godoc
// @Summary Create New HpBody.
// @Description Creating a new HpBody.
// @Tags HpBody
// @Param Body body hpBodyInput true "the body to create a new HpBody"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.HpBody
// @Router /hp-body [post]
func CreateHpBody(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// Validate input
	var input hpBodyInput
	var handphone models.Handphone
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := db.Where("id = ?", input.HandphoneID).First(&handphone).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "handphoneID not found!"})
		return
	}

	// Create Rating
	rating := models.HpBody{Dimensions: input.Dimensions, Weight: input.Weight, Build: input.Build, SIM: input.SIM, HandphoneID: input.HandphoneID}
	db.Create(&rating)

	c.JSON(http.StatusOK, gin.H{"data": rating})
}

// UpdateHpBody godoc
// @Summary Update HpBody.
// @Description Update HpBody by id.
// @Tags HpBody
// @Produce json
// @Param id path string true "HpBody id"
// @Param Body body hpBodyInput true "the body to update hp body"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.HpBody
// @Router /hp-body/{id} [patch]
func UpdateHpBody(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var hpBody models.HpBody
	var handphone models.Handphone
	if err := db.Where("id = ?", c.Param("id")).First(&hpBody).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input hpBodyInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := db.Where("id = ?", input.HandphoneID).First(&handphone).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "handphoneID not found!"})
		return
	}

	var updatedInput models.HpBody
	updatedInput.Dimensions = input.Dimensions
	updatedInput.Weight = input.Weight
	updatedInput.Build = input.Build
	updatedInput.SIM = input.SIM
	updatedInput.HandphoneID = input.HandphoneID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&hpBody).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": hpBody})
}

// DeleteHpBody godoc
// @Summary Delete one HpBody.
// @Description Delete a HpBody by id.
// @Tags HpBody
// @Produce json
// @Param id path string true "HpBody id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /hp-body/{id} [delete]
func DeleteHpBody(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var rating models.HpBody
	if err := db.Where("id = ?", c.Param("id")).First(&rating).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&rating)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
