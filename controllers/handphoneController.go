package controllers

import (
	"net/http"
	"time"

	"api-heroku/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type handphoneInput struct {
	Image string `json:"image"`
	Brand string `json:"brand"`
	Type  string `json:"type"`
	Year  string `json:"year"`
	Video string `json:"video"`
}

// GetAllHandphone godoc
// @Summary Get all Handphone.
// @Description Get a list of Handphone.
// @Tags Handphone
// @Produce json
// @Success 200 {object} []models.Handphone
// @Router /handphone [get]
func GetAllHandphone(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var ratings []models.Handphone
	db.Find(&ratings)

	c.JSON(http.StatusOK, gin.H{"data": ratings})
}

// CreateHandphone godoc
// @Summary Create New Handphone.
// @Description Creating a new Handphone.
// @Tags Handphone
// @Param Body body handphoneInput true "the body to create a new Handphone"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Handphone
// @Router /handphone [post]
func CreateHandphone(c *gin.Context) {
	// Validate input
	var input handphoneInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Rating
	rating := models.Handphone{Image: input.Image, Brand: input.Brand, Type: input.Type, Year: input.Year, Video: input.Video}
	db := c.MustGet("db").(*gorm.DB)
	result := db.Create(&rating)

	if result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": result.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": rating})
}

// GetHandphoneById godoc
// @Summary Get Handphone.
// @Description Get an Handphone by id.
// @Tags Handphone
// @Produce json
// @Param id path string true "Handphone id"
// @Success 200 {object} models.Handphone
// @Router /handphone/{id} [get]
func GetHandphoneById(c *gin.Context) { // Get model if exist
	var rating models.Handphone

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&rating).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": rating})
}

// UpdateHandphone godoc
// @Summary Update Handphone.
// @Description Update Handphone by id.
// @Tags Handphone
// @Produce json
// @Param id path string true "Handphone id"
// @Param Body body handphoneInput true "the body to update handphone"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Handphone
// @Router /handphone/{id} [patch]
func UpdateHandphone(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var rating models.Handphone
	if err := db.Where("id = ?", c.Param("id")).First(&rating).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input handphoneInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.Handphone
	updatedInput.Image = input.Image
	updatedInput.Brand = input.Brand
	updatedInput.Type = input.Type
	updatedInput.Year = input.Year
	updatedInput.Video = input.Video
	updatedInput.UpdatedAt = time.Now()

	db.Model(&rating).Updates(updatedInput)
	c.JSON(http.StatusOK, gin.H{"data": rating})
}

// DeleteHandphone godoc
// @Summary Delete one Handphone.
// @Description Delete a Handphone by id.
// @Tags Handphone
// @Produce json
// @Param id path string true "Handphone id"
// @Success 200 {object} map[string]boolean
// @Router /handphone/{id} [delete]
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
func DeleteHandphone(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var rating models.Handphone
	if err := db.Where("id = ?", c.Param("id")).First(&rating).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&rating)

	c.JSON(http.StatusOK, gin.H{"data": true})
}

// GetHpBodyByHandphoneId godoc
// @Summary Get HpBodies.
// @Description Get all HpBodies by HandphoneId.
// @Tags Handphone
// @Produce json
// @Param id path string true "Handphone id"
// @Success 200 {object} []models.HpBody
// @Router /handphone/{id}/hp-bodies [get]
func GetHpBodiesByHandphoneId(c *gin.Context) { // Get model if exist
	var hpBodies []models.HpBody

	db := c.MustGet("db").(*gorm.DB)

	if err := db.Where("handphone_id = ?", c.Param("id")).Find(&hpBodies).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": hpBodies})
}

// GetHpDisplayByHandphoneId godoc
// @Summary Get HpDisplays.
// @Description Get all HpDisplays by HandphoneId.
// @Tags Handphone
// @Produce json
// @Param id path string true "Handphone id"
// @Success 200 {object} []models.HpDisplay
// @Router /handphone/{id}/hp-displays [get]
func GetHpDisplaysByHandphoneId(c *gin.Context) { // Get model if exist
	var hpdisplays []models.HpDisplay

	db := c.MustGet("db").(*gorm.DB)

	if err := db.Where("handphone_id = ?", c.Param("id")).Find(&hpdisplays).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": hpdisplays})
}

// GetHpMemoryByHandphoneId godoc
// @Summary Get HpMemories.
// @Description Get all HpMemories by HandphoneId.
// @Tags Handphone
// @Produce json
// @Param id path string true "Handphone id"
// @Success 200 {object} []models.HpMemory
// @Router /handphone/{id}/hp-memories [get]
func GetHpMemoriesByHandphoneId(c *gin.Context) { // Get model if exist
	var hpMemories []models.HpMemory

	db := c.MustGet("db").(*gorm.DB)

	if err := db.Where("handphone_id = ?", c.Param("id")).Find(&hpMemories).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": hpMemories})
}

// GetHpPlatformByHandphoneId godoc
// @Summary Get HpPlatforms.
// @Description Get all HpPlatforms by HandphoneId.
// @Tags Handphone
// @Produce json
// @Param id path string true "Handphone id"
// @Success 200 {object} []models.HpPlatform
// @Router /handphone/{id}/hp-platforms [get]
func GetHpPlatformsByHandphoneId(c *gin.Context) { // Get model if exist
	var hpPlatforms []models.HpPlatform

	db := c.MustGet("db").(*gorm.DB)

	if err := db.Where("handphone_id = ?", c.Param("id")).Find(&hpPlatforms).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": hpPlatforms})
}
