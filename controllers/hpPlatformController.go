package controllers

import (
	"net/http"
	"time"

	"api-heroku/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type hpPlatformInput struct {
	OS          string `json:"os"`
	Chipset     string `json:"chipset"`
	CPU         string `json:"cpu"`
	GPU         string `json:"gpu"`
	HandphoneID string `json:"handphone_id"`
}

// GetAllHpPlatform godoc
// @Summary Get all HpPlatform.
// @Description Get a list of HpPlatform.
// @Tags HpPlatform
// @Produce json
// @Success 200 {object} []models.HpPlatform
// @Router /hp-platform [get]
func GetAllHpPlatform(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var ratings []models.HpPlatform
	db.Find(&ratings)

	c.JSON(http.StatusOK, gin.H{"data": ratings})
}

// GetHpPlatformById godoc
// @Summary Get HpPlatform.
// @Description Get an HpPlatform by id.
// @Tags HpPlatform
// @Produce json
// @Param id path string true "HpPlatform id"
// @Success 200 {object} models.HpPlatform
// @Failure 400 {object} models.HpPlatform
// @Router /hp-platform/{id} [get]
func GetHpPlatformById(c *gin.Context) { // Get model if exist
	var hpPlatform models.HpPlatform

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&hpPlatform).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": hpPlatform})
}

// CreateHpPlatform godoc
// @Summary Create New HpPlatform.
// @Description Creating a new HpPlatform.
// @Tags HpPlatform
// @Param Body body hpPlatformInput true "the body to create a new HpPlatform"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.HpPlatform
// @Router /hp-platform [post]
func CreateHpPlatform(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// Validate input
	var input hpPlatformInput
	var handphone models.Handphone
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := db.Where("id = ?", input.HandphoneID).First(&handphone).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "handphoneID not found!"})
		return
	}

	// Create Rating
	rating := models.HpPlatform{OS: input.OS, Chipset: input.Chipset, CPU: input.CPU, GPU: input.GPU, HandphoneID: input.HandphoneID}

	db.Create(&rating)

	c.JSON(http.StatusOK, gin.H{"data": rating})
}

// UpdateHpPlatform godoc
// @Summary Update HpPlatform.
// @Description Update HpPlatform by id.
// @Tags HpPlatform
// @Produce json
// @Param id path string true "HpPlatform id"
// @Param Body body hpPlatformInput true "the body to update age rating category"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.HpPlatform
// @Router /hp-platform/{id} [patch]
func UpdateHpPlatform(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var rating models.HpPlatform
	var handphone models.Handphone
	if err := db.Where("id = ?", c.Param("id")).First(&rating).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input hpPlatformInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := db.Where("id = ?", input.HandphoneID).First(&handphone).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "handphoneID not found!"})
		return
	}

	var updatedInput models.HpPlatform
	updatedInput.OS = input.OS
	updatedInput.Chipset = input.Chipset
	updatedInput.CPU = input.CPU
	updatedInput.GPU = input.GPU
	updatedInput.HandphoneID = input.HandphoneID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&rating).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": rating})
}

// DeleteHpPlatform godoc
// @Summary Delete one HpPlatform.
// @Description Delete a HpPlatform by id.
// @Tags HpPlatform
// @Produce json
// @Param id path string true "HpPlatform id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /hp-platform/{id} [delete]
func DeleteHpPlatform(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var rating models.HpPlatform
	if err := db.Where("id = ?", c.Param("id")).First(&rating).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&rating)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
