package controllers

import (
	"net/http"
	"time"

	"api-heroku/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type hpMemoryInput struct {
	Card_Slot   string `json:"card_slot"`
	Internal    string `json:"internal"`
	HandphoneID string `json:"handphone_id"`
}

// GetAllHpMemory godoc
// @Summary Get all HpMemory.
// @Description Get a list of HpMemory.
// @Tags HpMemory
// @Produce json
// @Success 200 {object} []models.HpMemory
// @Router /hp-memory [get]
func GetAllHpMemory(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var ratings []models.HpMemory
	db.Find(&ratings)

	c.JSON(http.StatusOK, gin.H{"data": ratings})
}

// GetHpMemoryById godoc
// @Summary Get HpMemory.
// @Description Get an HpMemory by id.
// @Tags HpMemory
// @Produce json
// @Param id path string true "HpMemory id"
// @Success 200 {object} models.HpMemory
// @Failure 400 {object} models.HpMemory
// @Router /hp-memory/{id} [get]
func GetHpMemoryById(c *gin.Context) { // Get model if exist
	var hpMemory models.HpMemory

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("id = ?", c.Param("id")).First(&hpMemory).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": hpMemory})
}

// CreateHpMemory godoc
// @Summary Create New HpMemory.
// @Description Creating a new HpMemory.
// @Tags HpMemory
// @Param Body body hpMemoryInput true "the body to create a new HpMemory"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.HpMemory
// @Router /hp-memory [post]
func CreateHpMemory(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	// Validate input
	var input hpMemoryInput
	var handphone models.Handphone
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := db.Where("id = ?", input.HandphoneID).First(&handphone).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "handphoneID not found!"})
		return
	}

	// Create Rating
	rating := models.HpMemory{Card_Slot: input.Card_Slot, Internal: input.Internal, HandphoneID: input.HandphoneID}
	db.Create(&rating)

	c.JSON(http.StatusOK, gin.H{"data": rating})
}

// UpdateHpMemory godoc
// @Summary Update HpMemory.
// @Description Update HpMemory by id.
// @Tags HpMemory
// @Produce json
// @Param id path string true "HpMemory id"
// @Param Body body hpMemoryInput true "the body to update hp memory"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.HpMemory
// @Router /hp-memory/{id} [patch]
func UpdateHpMemory(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	// Get model if exist
	var rating models.HpMemory
	if err := db.Where("id = ?", c.Param("id")).First(&rating).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input hpMemoryInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedInput models.HpMemory
	updatedInput.Card_Slot = input.Card_Slot
	updatedInput.Internal = input.Internal
	updatedInput.HandphoneID = input.HandphoneID
	updatedInput.UpdatedAt = time.Now()

	db.Model(&rating).Updates(updatedInput)

	c.JSON(http.StatusOK, gin.H{"data": rating})
}

// DeleteHpMemory godoc
// @Summary Delete one HpMemory.
// @Description Delete a HpMemory by id.
// @Tags HpMemory
// @Produce json
// @Param id path string true "HpMemory id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /hp-memory/{id} [delete]
func DeleteHpMemory(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var rating models.HpMemory
	if err := db.Where("id = ?", c.Param("id")).First(&rating).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&rating)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
